# anonymous-analytics

A privacy-friendly server-side analytics library.

## Getting started

### Install

From [npm](https://www.npmjs.com/package/anonymous-analytics)

- `yarn install anonymous-analytics`

or

- `npm install anonymous analytics`

### Usage

Create a new analytics database (e.g. named `test`).

```js
const
    Analytics = require('anonymous-analytics'),
    testAnalytics = new Analytics('test');
```

Populate the database with your endpoint's data.
- Default fingerprint : IP address (+ eventual reverse proxy IP) + user agent.
- Default extra payload : IP address country.

#### [Express](https://github.com/expressjs/express)

```js
const
    express = require('express'),
    app = express();
app.get('/test', async (request, response) => {
    await testAnalytics.addFromExpress(request);
    res.status(204).send();
});
```

#### [Fastify](https://github.com/fastify/fastify)

```js
const
    fastify = require('fastify'),
    app = fastify();
app.get('/test', async (request, reply) => {
    await testAnalytics.addFromFastify(request);
    reply.code(204).send();
});
```

#### Custom

```js
myTestHandler((myRequestContext, myResponseHandler) => {
    const {
        uniqueProperty1,
        uniqueProperty2,
        usefulPropertyA,
        usefulPropertyB
    } = myResponseHandler;
    testAnalytics.add(
        uniqueProperty1 + uniqueProperty2, // Fingerprint (string)
        { usefulPropertyA, usefulPropertyB } // Extra payload (object)
    );
    myResponseHandler(myResponseContext);
});
```

Read the data as stats.

```js
await testAnalytics.getStats(
    fromDayTimestamp,
    toDayTimestamp,
    convertToYYYYMMDD
);
```

```json
{
    "uniqueCount": 321,
    "totalCount": 654,
    "days": {
        "2020-01-01": {
            "uniqueCount": 123,
            "totalCount": 456
        }
    },
    "countries": {
        "France": {
            "uniqueCount": 12,
            "totalCount": 345
        }
    }
}
```

## FAQ

### How is user privacy protected ?

User fingerprints are hashed using SHA256 and timestamps are rounded to the day.

## Changelog

- `1.0.0` (2021-01-22) • Initial release