const
    DataStore = require('nedb'),
    { SHA256 } = require('jshashes'),
    fs = require('fs'),
    path = require('path'),
    ip2Country = require('@kxpvcr/ip-to-country');

const sha256 = new SHA256().hex;

try { fs.mkdirSync(path.join(require.resolve('@kxpvcr/ip-to-country'), '../data')) } catch(_){}

if(!ip2Country.dbLoad())
    ip2Country.dbGet().catch(console.error);

const getIpCountry = ip => {
    try {
        return ip2Country.ipSearch(ip).data[ip].country;
    } catch(_){}
};

module.exports = function(name){
    const db = new DataStore({
        filename: `data/${name}`,
        autoload: true
    });

    this.add = (fingerprint, extra) => new Promise((resolve, reject) => {
        db.update({
            hash: sha256(fingerprint)
        }, {
            $inc: {
                [`days.${new Date().setUTCHours(0, 0, 0, 0)}`]: 1
            },
            ...(extra ? { $set: extra } : {})
        }, {
            upsert: true,
            returnUpdatedDocs: true
        }, (error, n) => {
            if(n === 1)
                resolve();
            else
                reject(error);
        });
    });

    this.addFromExpress = async request => await this.add(
        request.headers['x-forwarded-for']
        + request.ip
        + request.connection.remoteAddress
        + request.headers['user-agent'],
        {
            country: getIpCountry(
                request.headers['x-forwarded-for']
                || request.ip
                || request.connection.remoteAddress
            )
        }
    );

    this.addFromFastify = async request => await this.add(
        request.headers['x-forwarded-for']
        + request.ip
        + request.raw.ip
        + request.headers['user-agent'],
        {
            country: getIpCountry(
                request.headers['x-forwarded-for']
                ||
                request.ip
                ||
                request.raw.ip
            )
        }
    );

    this.getStats = (
        fromDayTimestamp = -Infinity,
        toDayTimestamp = Infinity,
        convertToYYYYMMDD
    ) => new Promise((resolve, reject) => {
        db.find({}, (error, docs) => {
            if(error) reject(error);
            const res = {
                uniqueCount: 0,
                totalCount: 0,
                days: {},
                countries: {}
            };
            docs.forEach(doc => {
                const
                    days = Object.entries(doc.days).filter(([day]) => parseInt(day) >= fromDayTimestamp && parseInt(day) <= toDayTimestamp),
                    totalCount = days.reduce((a, [, count]) => a + count, 0),
                    { country } = doc;
                if(!days.length) return;
                res.uniqueCount++;
                res.totalCount += totalCount;
                days.forEach(([day, totalCount]) => {
                    if(convertToYYYYMMDD) day = new Date(parseInt(day)).toISOString().split('T')[0];
                    if(!res.days[day]) res.days[day] = {
                        uniqueCount: 1,
                        totalCount
                    };
                    else {
                        res.days[day].uniqueCount++;
                        res.days[day].totalCount += totalCount;
                    }
                });
                if(country){
                    if(!res.countries[country]) res.countries[country] = {
                        uniqueCount: 1,
                        totalCount
                    }
                    else {
                        res.countries[country].uniqueCount++;
                        res.countries[country].totalCount += totalCount;
                    }
                }
            });
            resolve(res);
        });
    });
};

module.exports.getIpCountry = getIpCountry;